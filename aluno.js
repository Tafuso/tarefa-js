let notasAluno = [5, 0, 6]

function calcularMedia() {
  let i = 0
  notasAluno.forEach(nota => i += nota)

  let mediaAritmetica = i/notasAluno.length

  if (mediaAritmetica >= 6) return 'aprovado'
  else return 'reprovado'

}

console.clear()
console.log('---------------------------------------')
console.log(`O aluno foi ${calcularMedia()}.`)
console.log('---------------------------------------')