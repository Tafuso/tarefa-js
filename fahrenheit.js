var fahrenheit = 20

function conversion(x) {
  var celsius = ((x - 32) / 9)*5

  return celsius
}

console.clear()
console.log('---------------------------------------')
console.log(`${fahrenheit} °F equivale a ${conversion(fahrenheit).toFixed(2)} °C`)
console.log('---------------------------------------')
