let numeros = [100, 55, 44, 33, 99]

console.clear()
console.log('---------------------------------------')
function fizzbuzz() {
  numeros.forEach(numero => {
    if ((numero % 3 === 0) && (numero % 5 === 0)) {
      console.log('fizzbuzz')
    } else if(numero % 3 === 0) {
      console.log('fizz')
    } else if (numero % 5 === 0) {
      console.log('buzz')
    } else {
      console.log(numero)
    }
  })

  return null
}
fizzbuzz()
console.log('---------------------------------------')
